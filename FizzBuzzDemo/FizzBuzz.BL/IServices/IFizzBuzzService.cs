﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Core.Services
{
     public interface IFizzBuzzService
    {
        List<string> FizzBuzz(int number);
       
    }
}
