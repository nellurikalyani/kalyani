﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.BL
{
   public class FizzBuzzDivisible
    {
        public string GetDivisibleByThreeAndFiveValue(int Number)
        {
            return "FizzBuzz";
        }
        public string GetDivisibleByThreeValue(int Number)
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                return "Wizz";
            return "Fizz";

        }

        public string GetDivisibleByFiveValue(int Number)
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                return "Wuzz";
            return "Buzz";

        }
       

    }
}
