﻿using System;

namespace FizzBuzz.Core
{
    public class FizzBuzzCheck
    {
        public string IsDivisibleByThreeAndFive(int Number)
        {            
            return DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "WizzWuzz" : "FizzBuzz";
        }
        public string IsDivisibleByThree(int Number)
        {
            return DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "Wizz" : "Fizz";
        }

        public string IsDivisibleByFive(int Number)
        {
            return DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "Wuzz" : "Buzz";
            
        }
       

    }
}
