﻿using System;
using System.Collections.Generic;

namespace FizzBuzz.Core.Services
{
    public class FizzBuzzService : IFizzBuzzService
    {
        public List<string> FizzBuzz(int number)
        {
            var result = new List<string>();
            for ( int positivenumber = 1; positivenumber <= number; positivenumber++)
            {
                var fizzbuzzresult = new FizzBuzzCheck();
                string resultValue;
                resultValue = positivenumber % 15 == 0 ? fizzbuzzresult.IsDivisibleByThreeAndFive(positivenumber)
                    : positivenumber % 3 == 0 ? fizzbuzzresult.IsDivisibleByThree(positivenumber)
                    : positivenumber % 5 == 0 ? fizzbuzzresult.IsDivisibleByFive(positivenumber)
                    : positivenumber.ToString();
                result.Add(resultValue);
            }
            return result;
        }

       
    }
}


