﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Core.Test
{
    [TestFixture]
    public class FizzBuzzCheckTest
    {
        private FizzBuzzCheck GetCheck()
        {
            return new FizzBuzzCheck();
        }
        [Test]
        public void CanCreateCheck()
        {
            Assert.IsNotNull(GetCheck());
        }
        [Test]
        public void FizzBuzz_Should_Return_Fizz_Or_Wizz_If_Divisible_By_Three()
        {

            // Arrange
            
            var expected = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday?"Wizz":"Fizz";

            // Act
            var actual = GetCheck().IsDivisibleByThree(3);

            // Assert
            CollectionAssert.AreEqual(expected, actual);

        }
        [Test]
        public void FizzBuzz_Should_Return_Buzz_Or_Wuzz_If_Divisible_By_Five()
        {

            // Arrange
            var expected = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "Wuzz" : "Buzz";

            // Act
            var actual = GetCheck().IsDivisibleByFive(5);

            // Assert
            CollectionAssert.AreEqual(expected, actual);

        }

        [Test]
        public void FizzBuzz_Should_Return_Buzz_If_Divisible_By_Three_And_Five()
        {
            // Arrange
            var expected = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "WizzWuzz" : "FizzBuzz"; ;

            // Act
            var actual = GetCheck().IsDivisibleByThreeAndFive(15);

            // Assert
            CollectionAssert.AreEqual(expected, actual);

        }
    }
}
