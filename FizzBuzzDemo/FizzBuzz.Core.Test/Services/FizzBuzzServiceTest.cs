﻿using FizzBuzz.Core.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace FizzBuzz.Core.Test
{
    [TestFixture]
    public class FizzBuzzServiceTest
    {

        private FizzBuzzService GetService()
        {
            return new FizzBuzzService();
        }
        [Test]
        public void CanCreateService()
        {
            Assert.IsNotNull(GetService());
        }

        [Test]
        public void FizzBuzz_Should_Return_List_With_One_Two_If_Input_Is_Two()
        {
            var mockFizzBuzzService = new Mock<IFizzBuzzService>();
            // Arrange
            var expected = new List<string> { "1", "2" };
            mockFizzBuzzService.Setup(x => x.FizzBuzz(2)).Returns(()=>expected);
            //Act
            var res = mockFizzBuzzService.Object.FizzBuzz(2);
            // Assert
            Assert.AreEqual(expected,res);
        }

        [Test]
        public void FizzBuzz_Should_Return_List_With_One_Two_Fizz_If_Input_Is_Three()
        {
            // Arrange
            
            var expected = DateTime.Now.DayOfWeek==DayOfWeek.Wednesday ? new List<string> { "1", "2", "Wizz" }: new List<string> { "1", "2", "Fizz" };

            // Act
            var actual = GetService().FizzBuzz(3);

            // Assert
            CollectionAssert.AreEqual(expected, actual);

        }

        [Test]
        public void FizzBuzz_Should_Return_List_With_Buzz_If_Input_Is_Five()
        {
            // Arrange

            var expected = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? new List<string> { "1", "2", "Wizz","4","Wuzz" } : new List<string> { "1", "2", "Fizz","4","Buzz" };

            // Act
            var actual = GetService().FizzBuzz(5);

            // Assert
            CollectionAssert.AreEqual(expected, actual);

        }
        [Test]
        public void FizzBuzz_Should_Return_List_With_FizzBizz_If_Input_Is_15_Or_Greater()
        {
            // Arrange
            var expected = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? 
                new List<string> { "1", "2", "Wizz","4","Wuzz","Wizz","7","8","Wizz","Wuzz","11","Wizz","13","14","WizzWuzz" }
                : new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz" };
            // Act
            var actualFizzBuzzList = GetService().FizzBuzz(15);
            
            // Assert
            Assert.AreEqual(expected, actualFizzBuzzList);
        }


      
    }
}
