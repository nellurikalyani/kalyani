﻿using FizzBuzz.Core.Services;
using NUnit;
using NUnit.Framework;
using Moq;

namespace FizzBuzz.Core.Tests.Services
{
    
    [TestFixture]
    public class FizzBuzzServiceTests
    {
        private FizzBuzzService GetService()
        {
            return new FizzBuzzService();
        }
        [Test]
        public void CanCreateService()
        {
            Assert.IsNotNull(GetService());
        }
    }

}
