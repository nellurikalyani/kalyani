﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.MVC.Web.ViewModels
{
    public class FizzBuzzModel
    {
        [Required, Range(1, 1000)]
        public int Number { get; set; }

        public IPagedList<String> Result
        {
            get; set;
        }
    }
}