﻿using FizzBuzz.Core.Services;
using FizzBuzz.MVC.Web.ViewModels;
using PagedList;
using System;
using System.Web.Mvc;

namespace FizzBuzz.MVC.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFizzBuzzService fizzBuzzService;
        
        public HomeController(IFizzBuzzService fizzBuzzServices)
        {
            fizzBuzzService = fizzBuzzServices;
        }

        public ActionResult Index()
        {
            return View("Index");
        }

        [HttpPost]
        public ActionResult Index(FizzBuzzModel fizzBuzzModel, int? page)
        {
            var number =1 ;
            if (number < 1 || number > 1000)
            {
                throw new ArgumentOutOfRangeException($"Value must be between 1 and 1000 but was {number}");
            }
            if (!ModelState.IsValid)
            {
                fizzBuzzModel.Result = null;
                return View(fizzBuzzModel);
            };
             var pageSize = 20;
            var pageNumber = (page ?? 1);

            fizzBuzzModel.Result = fizzBuzzService.FizzBuzz(fizzBuzzModel.Number).ToPagedList(pageNumber, pageSize);

            //return RedirectToAction("FizzBuzz", new { value = fizzBuzzModel.Number, page = 1 });
            return View ("FizzBuzz", fizzBuzzModel);
        }

        [HttpGet]
        public ActionResult FizzBuzz(int value, int? page)
        {
           
            var fizzBuzzModel = new FizzBuzzModel();
            fizzBuzzModel.Number = value;
            var pageSize = 20;
            var pageNumber = (page ?? 1);


            fizzBuzzModel.Result = fizzBuzzService.FizzBuzz(fizzBuzzModel.Number).ToPagedList(pageNumber, pageSize);

            return View(fizzBuzzModel);


        }
    }
}
