﻿using FizzBuzz.Core.Services;
using FizzBuzz.MVC.Web.Controllers;
using FizzBuzz.MVC.Web.ViewModels;
using NUnit.Framework;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FizzBuzz.MVC.Web.Tests
{
    [TestFixture]
    public class HomeControllerTest
    {
        private IFizzBuzzService service;

        [Test]
        public void Index_Action_Returns_Index_View()
        {
            service = new FizzBuzzService();
            var controller = new HomeController(service);
            Assert.IsNotNull(controller);
        }

        [Test]
        public void Test_Home_Index()
        {
            service = new FizzBuzzService();
            var obj = new HomeController(service);
            var actResult = obj.Index() as ViewResult;
            Assert.That(actResult.ViewName, Is.EqualTo("Index"));
        }
        [Test]
        public void Test_Home_Redirect_FizzBuzz()
        {
           service = new FizzBuzzService();
            var obj = new HomeController(service);
            var result = obj.Index(new FizzBuzzModel()
            {
                Number = 10
            },1) as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("FizzBuzz"));
        }
    }
}


